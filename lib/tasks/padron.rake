require 'open-uri'

namespace :padron do
  desc "update padron from google url"
  task :update_from_google, [:url] => [:environment] do |t, args|
    p args[:url]
    url = 'https://docs.google.com/spreadsheets/d/16-wehtxMK6OnVRElcL3mRX9a61lNFDt6m2he9WpWs_s/gviz/tq?tqx=out:csv'

    if args[:url]
      url = args[:url]
    end

    AllowedId.populate_from_file(file_path: "",url: url)
  end

end
