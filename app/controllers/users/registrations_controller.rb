class Users::RegistrationsController < Devise::RegistrationsController
  prepend_before_action :authenticate_scope!, only: [:edit, :update, :destroy, :finish_signup, :do_finish_signup]
  before_action :configure_permitted_parameters

  invisible_captcha only: [:create], honeypot: :address, scope: :user

  def new
    super do |user|
      user.use_redeemable_code = true if params[:use_redeemable_code].present?
    end
  end

  def create
    rut = sign_up_params['username']
    code = sign_up_params['password']
    auth, mess = auth_with_citizen(rut, code, true)

    if auth
      parms = params
      parms[:user][:username] = mess
      parms[:user][:password_confirmation] = code
      parms[:user][:confirmed_at] = Time.now
      parms[:user][:verified_at] = Time.now
      params = parms
    else
      flash[:error] = mess
      message = mess
    end
    build_resource(sign_up_params)
    if resource.valid? and auth
      p 'resource was valid'
      super
    else
      p 'resource not valid'
      render :new
    end
  end

  def auth_with_citizen(rut, code, just_check_number=false)
    if not just_check_number
      uri = URI('http://farm.autentificacion.xyz/verify')
      params = { :rut => rut ,:number => code }
      uri.query = URI.encode_www_form(params)
      res = Net::HTTP.get_response uri

      if res.is_a?(Net::HTTPOK)
        p 'response was ok'
        p JSON.parse(res.body)
        res_json = JSON.parse(res.body)
        if res_json['status'] == 'success' and res_json['padron'] == true
          p 'autentification was success'
          return true, res_json['rut']
        elsif res_json['status'] == 'success' and res_json['padron'] == false
          mess = 'Lo sentimos, usted no está inscrito en el padrón electoral de la comuna de Renca.'
          return false, mess
        else
          mess = 'La autentificación falló o no estas habilitado para votar.'
          return false, mess
        end
      else
        mess = 'Algo falló, por favor intentalo mas tarde.'
        return false, mess
      end
    else
      raw_rut, dig, clean_rut = Users::RegistrationsController.clean_rut(rut)

      if (AllowedId.find_by rut: clean_rut).nil?
        return false, 'No te encuentras en el padrón, Contactate con la mesa de ayuda para arreglarlo.'
      end

      if Users::RegistrationsController.digito_verificador(raw_rut).to_s == dig and check_number(code)
        return true, clean_rut
      else
        return false, 'La verificación fallo, porfavor revisa tus datos.'
      end
    end
  end

  def self.clean_rut(rut)
    raw_rut = rut.to_s.downcase.strip.gsub(/-|\.|\*| /, "")
    raw_rut = raw_rut[0..raw_rut.length-2]
    dig = rut.to_s.downcase[rut.length-1..rut.length-1]
    clean_rut = raw_rut.to_s + '-' +dig.to_s
    return raw_rut, dig, clean_rut
  end

  def self.digito_verificador(rut)
    return  [*0..9,'k'][rut.to_s.reverse.chars.inject([0,0]){|(i,a),n|[i+1,a-n.to_i*(i%6+2)]}[1]%11]
  end

  def check_number(code)
    n_code = code.to_s[1..code.length-1]
    return !!(n_code =~ /\A[-+]?[0-9]+\z/)
  end


  def delete_form
    build_resource({})
  end

  def delete
    current_user.erase(erase_params[:erase_reason])
    sign_out
    redirect_to root_url, notice: t("devise.registrations.destroyed")
  end

  def success
  end

  def finish_signup
    current_user.registering_with_oauth = false
    current_user.email = current_user.oauth_email if current_user.email.blank?
    current_user.validate
  end

  def do_finish_signup
    current_user.registering_with_oauth = false
    if current_user.update(sign_up_params)
      current_user.send_oauth_confirmation_instructions
      sign_in_and_redirect current_user, event: :authentication
    else
      render :finish_signup
    end
  end

  def check_username
    rut = params[:username]
    raw_rut = rut.to_s.strip.gsub(/-|\.|\*| /, "")
    dig = rut[rut.length-1..rut.length-1]
    rut = raw_rut[0..raw_rut.length-2] + '-' +  dig
    if User.find_by username: rut
      render json: {available: false, message: t("devise_views.users.registrations.new.username_is_not_available")}
    else
      render json: {available: true, message: t("devise_views.users.registrations.new.username_is_available")}
    end
  end

  private

    def sign_up_params
      params[:user].delete(:redeemable_code) if params[:user].present? && params[:user][:redeemable_code].blank?
      params.require(:user).permit(:username, :email, :password,
                                   :password_confirmation, :terms_of_service, :locale,
                                   :redeemable_code, :confirmed_at, :verified_at)
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:account_update, keys: [:username])
    end

    def erase_params
      params.require(:user).permit(:erase_reason)
    end

    def after_inactive_sign_up_path_for(resource_or_scope)
      users_sign_up_success_path
    end

end
