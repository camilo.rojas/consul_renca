require 'csv'
require 'open-uri'

class AllowedId < ApplicationRecord

  validate :validate_rut

  def validate_rut
    raw_rut, dig, clean_rut = Users::RegistrationsController.clean_rut self.rut

    if dig.to_s ==  Users::RegistrationsController.digito_verificador(raw_rut).to_s
      self.rut = clean_rut
    else
      p 'Invalid rut: ' + self.rut.to_s
      errors.add(:rut, 'Invalid rut')
    end
  end

  def self.populate_from_file(file_path: 'padron/example.csv', url: nil)
    if url.nil?
      data = CSV.read(file_path, headers: false)
    else
      data = CSV.parse(open(url))
    end

    AllowedId.delete_all
    n_data = data.size

    data.each_with_index do | row, index |
      AllowedId.create(rut: row[0])
      if index % 1000 == 0
        p 'Loaded: ' +  index.to_s + " of " + n_data.to_s
      end
    end
    p data.count
  end

end
