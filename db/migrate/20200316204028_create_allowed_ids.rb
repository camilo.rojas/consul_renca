class CreateAllowedIds < ActiveRecord::Migration[5.0]
  def change
    create_table :allowed_ids do |t|
      t.string :rut
    end

    add_index :allowed_ids, :rut, unique: true
  end
end
